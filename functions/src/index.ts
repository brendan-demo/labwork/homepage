import * as functions from 'firebase-functions';
import * as admin       from 'firebase-admin';
admin.initializeApp();

const sha1 = require('sha1');
const urlMetadata = require('url-metadata');

import { goto } from './go';

export const addShortner = functions.https.onCall(async (data: any, context: any) => {
    const { requestedShort, longurl } = data;
    if (!context.auth) throw new functions.https.HttpsError('failed-precondition', 'The function must be called while authenticated.');
    if (!longurl) throw new functions.https.HttpsError('invalid-argument', 'longurls is missing');

    const sha: string       = getSHA(longurl);
    const shortcode: string = getShort(requestedShort, sha);

    const snap = await admin.firestore().collection('urls').doc(shortcode).get();
    if (snap.exists) throw new functions.https.HttpsError('already-exists', 'That short URL already exisits');

    const doc = await urlMetadata(longurl);
    console.log(doc);
    let linktitle = doc.title;
    if (!linktitle) linktitle = '...unknown';

    return snap.ref.set({
        userId: context.auth.uid,
        longurl,
        shortcode,
        sha,
        linktitle,
        createdAt: admin.firestore.Timestamp.now(),
    })
});

export const handleGoURLs = goto;


function getSHA(longurl: string): string {
    const n = new Date().getTime();
    return sha1(`${longurl}#${n}`).toString();
}

function getShort(requestedShort: string, sha: string): string {
    if (requestedShort) return requestedShort;
    return sha.substring(0, 6);
}